<?php
include "includes/functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Pokedex</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/index.css">
    <link rel="icon" href="stuff/pokeball.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <header>
        <?php indexHeader(); ?>
    </header>
    <a href="filter.php" id='filter_btn' class='btn'>Filter pokemons</a>

    <?php
      if (isset($_SESSION['username'])) {
        echo "<a href='collection.php' id='colletion_btn' class='btn'>My collection</a>";

      } else {
        echo "<a href='login.php' id='colletion_btn' class='btn' title='You must be logged in to view your collection'>My collection</a>";

      }
    ?>
    <form action="search.php" id='searchform' method='post'>
      <input class='search btn' id='pokebtn'  type='submit' name='search_submit' value='Search'>
      <input class='search' id='pokeinput' type='text' name='search' placeholder='Search pokemon'>
    </form>

    <div class="content">
      <?php allPokemons(); ?>
    </div>
  </body>
</html>
