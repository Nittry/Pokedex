<?php
include "includes/functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Pokedex</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/pokemon.css">
    <link rel="icon" href="stuff/pokeball.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <header>
        <?php indexHeader(); ?>
    </header>

    <!-- <a href='index.php'><button id='back_btn'>BACK</button></a> -->
    <?php
      if (isset($_SESSION['id'])) {
        $user_id = $_SESSION['id'];
        $pokemon_id = mysqli_real_escape_string($conn,$_GET['id']);
        $pokemon_ids = array();
        //Array with pokemon in your collection
        $query = "SELECT id_pokemon FROM user_pokemons WHERE id_user={$user_id}";
        $result = mysqli_query($conn,$query);
        while ($row = mysqli_fetch_assoc($result)) {
          array_push($pokemon_ids,$row['id_pokemon']);
        }
        //Adding pokemon to your collection
        if (isset($_POST['add'])) {
          $query = "INSERT INTO user_pokemons(id_user, id_pokemon) VALUES ({$user_id},{$pokemon_id})";
          $result = mysqli_query($conn,$query);
          header("Refresh:0");
        }
        //Removeing pokemon from your collection
        if (isset($_POST['remove'])) {
          $query = "DELETE FROM user_pokemons WHERE id_user={$user_id} AND id_pokemon={$pokemon_id}";
          $result = mysqli_query($conn,$query);
          header("Refresh:0");
        }

      }


    ?>
    <?php
    //ECHO Image
    $query = "SELECT pokemons.name FROM pokemons WHERE id={$_GET['id']}";
    $result = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($result);
    echo "<div class='container'><div class='left_side'>";
    echo "<img id='poke_profil' src='poke_img/{$_GET['id']}.png' alt='Picture not found.'>";
    //If current pokemon is in your collection
    if(isset($_SESSION['id'])){
      if (in_array($pokemon_id,$pokemon_ids)) {
        echo "<form method='post'><input class='remove_poke' type='submit' name='remove' value='Remove pokemon from collection'></form>";
      }else{
        echo "<form method='post'><input class='add_poke' type='submit' name='add' value='Add pokemon to collection'></form>";
      }
    }
    //ECHO NAME
    echo "</div><div class='right_side'><h1>{$row['name']}</h1>";
    //ECHO EVOLUTION
    $query = "SELECT pokemons.evolution FROM pokemons WHERE id={$_GET['id']}";
    $result = $result = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($result);
    if($row['evolution']==1){
    echo "
          <img title='Evolution' class='evol' src='stuff/evolution_full.png'>
          <img title='Evolution' class='evol' src='stuff/evolution_empty.png'>
          <img title='Evolution' class='evol' src='stuff/evolution_empty.png'>
    ";
    }
    if($row['evolution']==2){
    echo "
          <img title='Evolution' class='evol' src='stuff/evolution_full.png'>
          <img title='Evolution' class='evol' src='stuff/evolution_full.png'>
          <img title='Evolution' class='evol' src='stuff/evolution_empty.png'>
    ";
    }
    if($row['evolution']==3){
    echo "
          <img title='Evolution' class='evol' src='stuff/evolution_full.png'>
          <img title='Evolution' class='evol' src='stuff/evolution_full.png'>
          <img title='Evolution' class='evol' src='stuff/evolution_full.png'>
    ";
    }
    //ECHO TYPES
    $query = "SELECT types.name
              FROM pokemons_types
              JOIN types ON types.id=pokemons_types.id_types
              JOIN pokemons ON pokemons.id=pokemons_types.id_pokemon
              WHERE pokemons.id={$_GET['id']}";
    $result = mysqli_query($conn,$query);
    echo "<h2>Type:</h2><div id='type_tab'>";
    while ($row = mysqli_fetch_row($result)) {
        foreach ($row as $value) {
          echo "
          <a href='search.php?type={$value}' class='type_name' id='{$value}'>{$value}<img class='type_img' src='types/{$value}.png'></a>
          ";
        }
    }
    echo "</div>";
    //ECHO WEAKNESSES
    $query = "SELECT types.name
                FROM pokemon_weaknesses
                JOIN types ON types.id=pokemon_weaknesses.id_weaknesses
                JOIN pokemons ON pokemons.id=pokemon_weaknesses.id_pokemon
                WHERE pokemons.id={$_GET['id']}";
    $result = mysqli_query($conn,$query);
    echo "<h2>Weaknesses:</h2><div id='weaknesses'>";
    while($row = mysqli_fetch_row($result)) {
        foreach ($row as $value) {
          echo "
                <a href='search.php?weakness={$value}' class='type_name' id='{$value}'>{$value}<img class='type_img' src='types/{$value}.png'></a>
          ";
        }
    }
    echo "</div></div></div>";
    ?>
  </body>
