<?php
include "../includes/functions.php";
include "admin_functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <script src='../js/jquery-3.3.1.min.js'></script>
  <script src='../js/main.js'></script>
  <link rel="stylesheet" href="../css/admin.css">
  <link rel="icon" href="../stuff/pokeball.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <title>Pokedex</title>
</head>
  <body>
    <img id='nav_icon' src='../stuff/nav_icon.png'>
    <a href='admin.php'><button id='back_btn'>BACK</button></a>
    <div id='bg_nav'>
      <?php echo "<h1 id='username'>{$_SESSION['username']}</h1>"; ?>
      <nav>
        <ul>
          <li class='lvl1'>Admins</li>
          <ul>
            <a href='admin.php'><li class='lvl2'>Dashboard</li></a>
            <a href='all_admins.php'><li class='lvl2'>All Admins</li></a>
          </ul>
        </ul>
        <ul>
          <li class='lvl1'>Users</li>
          <ul>
            <a href='all_users.php'><li class='lvl2'>All Users</li></a>
            <a href='change_username.php'><li class='lvl2'>Change Username</li></a>
            <a href='change_password.php'><li class='lvl2'>Change Password</li></a>
          </ul>
          <li class='lvl1'>Pokemons</li>
          <ul>
            <a href='all_pokemons.php'><li class='lvl2'>All Pokemons</li></a>
            <a href='create_pokemons.php'><li class='lvl2'>Create New Pokemon</li></a>
            <a href='all_types.php'><li class='lvl2'>All Pokemon Types</li></a>
            <a href='create_types.php'><li class='lvl2'>Create New Type</li></a>
          </ul>
        </ul>
      </nav>
    </div>

    <div class='content'>
      <h2>Edit</h2>
      <?php
      $query = "SELECT id,name FROM types;";
      $result = mysqli_query($conn,$query);
      $types=array();
      $types_id=array();
      $selected_type_id = array();
      $selected_type_name = array();
      $selected_weaknesses_id = array();
      $selected_weaknesses_name = array();
      while($row = mysqli_fetch_assoc($result)){
            array_push($types,$row['name']);
            array_push($types_id,$row['id']);
      }
      $query = "SELECT types.id, types.name
                FROM types JOIN pokemons_types ON types.id = pokemons_types.id_types
                WHERE pokemons_types.id_pokemon={$_GET['id']};";
      $result = mysqli_query($conn,$query);
      while($row = mysqli_fetch_assoc($result)){
         array_push($selected_type_id,$row['id']);
         array_push($selected_type_name,$row['name']);
      }

      $query = "SELECT types.id, types.name
                FROM types
                JOIN pokemon_weaknesses ON types.id = pokemon_weaknesses.id_weaknesses
                WHERE pokemon_weaknesses.id_pokemon={$_GET['id']};";
      $result = mysqli_query($conn,$query);
      while($row = mysqli_fetch_assoc($result)){
         array_push($selected_weaknesses_id,$row['id']);
         array_push($selected_weaknesses_name,$row['name']);
      }

      if(isset($_POST['submit'])){
        $name = mysqli_real_escape_string($conn,$_POST['name']);
        $evolution = mysqli_real_escape_string($conn,$_POST['evolution']);
        $types1= mysqli_real_escape_string($conn,$_POST['types1']);
        $types2 = mysqli_real_escape_string($conn,$_POST['types2']);
        $weaknesses = array();
        foreach ($types_id as $value) {
          if(isset($_POST[$value])){
            array_push($weaknesses,$_POST[$value]);
          }
        }
        $error = false;
        if(empty($name)){
          echo "<p class='err'>Pokemon's name cannot be empty!</p>";
          $error = true;
        }
        if (preg_match('/\s/',$name)) {
          echo "<p class='err'>Pokemon's name cannot contain white spaces.</p>";
          $error = true;
        }
        if(empty($evolution)){
          echo "<p class='err'>Pokemon's evolution cannot be empty!</p>";
          $error = true;
        }
        if (preg_match('/\s/',$evolution)) {
          echo "<p class='err'>Pokemon's evolution cannot contain white spaces.</p>";
          $error = true;
        }

        if(!$error){

          $query = "UPDATE pokemons SET name='{$name}',evolution={$evolution} WHERE pokemons.id={$_GET['id']}";
          $update_pokemon = mysqli_query($conn,$query);

          $query = "DELETE FROM pokemons_types WHERE id_pokemon={$_GET['id']}";
          $delete_pokemon_types = mysqli_query($conn,$query);

          if($types2 == 'nothing'){
            $query = "INSERT INTO pokemons_types(id_pokemon, id_types) VALUES ({$_GET['id']},{$types1})";
            $update_pokemon_types = mysqli_query($conn,$query);
          }else{
          $query = "INSERT INTO pokemons_types(id_pokemon, id_types) VALUES ({$_GET['id']},{$types1})";
          $update_pokemon_types = mysqli_query($conn,$query);
          $query = "INSERT INTO pokemons_types(id_pokemon, id_types) VALUES ({$_GET['id']},{$types2})";
          $update_pokemon_types = mysqli_query($conn,$query);
          }
          $query = "DELETE FROM pokemon_weaknesses WHERE id_pokemon={$_GET['id']}";
          $delete_pokemon_weaknessses = mysqli_query($conn,$query);
          foreach ($weaknesses as $weakness) {
            $query = "INSERT INTO pokemon_weaknesses(id_pokemon, id_weaknesses) VALUES ({$_GET['id']},{$weakness})";
            $update_pokemon_weaknessses = mysqli_query($conn,$query);
          }
          if($update_pokemon && $delete_pokemon_types  && $update_pokemon_types && $delete_pokemon_weaknessses && $update_pokemon_weaknessses){
            echo "Update successful";
          }
        }
      }
        echo "
              <form method='post'>
                <label class='change_label_purple'>Pokemon's name</label><br>
                <input class='change_input_purple this_inpt' name='name' type='text' value='{$_GET['name']}'><br>
                <label class='change_label_purple'>Evolution</label><br>
                <input class='change_input_purple number_input this_inpt' name='evolution' type='number' min='1' max='3' value='{$_GET['evolution']}'><br>
        ";

        echo "<i class='tip'>TIP: If your pokemon has only one type, set the second select for type2 to -nothing-.</i>";
        echo "<table class='margin_help'>";
        echo "<tr><th>1st Type</th><th>2nd Type</th></tr>";
        echo "<tr><td><select class='select_inpt' name='types1'>";
        $count_id=0;
        foreach ($types as $type) {
          if($type==$selected_type_name[0]){
            echo "<option class='option_inpt' value='{$types_id[$count_id]}' selected>{$type}</option>";
            $count_id++;
          }else{
            echo "<option class='option_inpt' value='{$types_id[$count_id]}'>{$type}</option>";
            $count_id++;
          }
        }
        echo "</select></td>";
        echo "<td><select class='select_inpt' name='types2'>";
        echo "<option class='option_inpt' value='nothing'>nothing</option>";
        $count_id=0;
        foreach ($types as $type) {
          if($type==$selected_type_name[1]){
            echo "<option class='option_inpt' value='{$types_id[$count_id]}' selected>{$type}</option>";
            $count_id++;
          }else{
            echo "<option value='{$types_id[$count_id]}'>{$type}</option>";
            $count_id++;
          }
        }
        echo "</select></td></tr></table>";
        echo "</table><table class='margin_help'>";
        echo "<i class='tip'>TIP: Every pokemon can have 0-5 weaknesses.</i>";
        echo "<tr><th colspan='2'>Weaknesses</th><th></th></tr>";
        $count_id=0;
        array_push($selected_weaknesses_name,"");
        foreach ($types as $type) {
          if(in_array($type,$selected_weaknesses_name)){
            echo "<tr><td class='box_td'><input class='box' type='checkbox' name='{$types_id[$count_id]}' value='{$types_id[$count_id]}' checked></td><td>{$type}</td></tr>";
            $count_id++;
          }else{
            echo "<tr><td class='box_td'><input class='box' type='checkbox' name='{$types_id[$count_id]}' value='{$types_id[$count_id]}'></td><td>{$type}</td></tr>";
            $count_id++;
          }
        }
        echo "</table>";
        echo "<input class='conf_btn_purple this_btn margin_help' name='submit' type='submit' value='Update your pokemon'></form>";
      ?>
    </div>
  </body>
</html>
