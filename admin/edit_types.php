<?php
include "../includes/functions.php";
include "admin_functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <script src='../js/jquery-3.3.1.min.js'></script>
  <script src='../js/main.js'></script>
  <link rel="stylesheet" href="../css/admin.css">
  <link rel="icon" href="../stuff/pokeball.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <title>Pokedex</title>
</head>
  <body>
    <img id='nav_icon' src='../stuff/nav_icon.png'>
    <a href='all_types.php'><button id='back_btn'>BACK</button></a>
    <div id='bg_nav'>
      <?php echo "<h1 id='username'>{$_SESSION['username']}</h1>"; ?>
      <nav>
        <ul>
          <li class='lvl1'>Admins</li>
          <ul>
            <a href='admin.php'><li class='lvl2 active'>Dashboard</li></a>
            <a href='all_admins.php'><li class='lvl2'>All Admins</li></a>
          </ul>
        </ul>
        <ul>
          <li class='lvl1'>Users</li>
          <ul>
            <a href='all_users.php'><li class='lvl2'>All Users</li></a>
            <a href='change_username.php'><li class='lvl2'>Change Username</li></a>
            <a href='change_password.php'><li class='lvl2'>Change Password</li></a>
          </ul>
          <li class='lvl1'>Pokemons</li>
          <ul>
            <a href='all_pokemons.php'><li class='lvl2'>All Pokemons</li></a>
            <a href='create_pokemons.php'><li class='lvl2'>Create New Pokemon</li></a>
            <a href='all_types.php'><li class='lvl2'>All Pokemon Types</li></a>
            <a href='create_types.php'><li class='lvl2'>Create New Type</li></a>
          </ul>
        </ul>
      </nav>
    </div>

    <div class='content'>
      <h2>Edit Types</h2>
      <?php
      if (isset($_POST['submit'])) {
        $name = mysqli_real_escape_string($conn,$_POST['name']);
        $query = "UPDATE types SET name='{$name}' WHERE id={$_GET['id']}";
        $result = mysqli_query($conn,$query);
        if ($result) {
          echo "Data Updated";
        } else {
          echo "ERROR";
        }

      }
        $query = "SELECT name FROM types WHERE id={$_GET['id']}";
        $result = mysqli_query($conn,$query);
        $row = mysqli_fetch_assoc($result);

        echo "<form method='post'>";
        echo "<label class='change_label_purple'>Type Name</label><input name='name' type='text' value='{$row['name']}' class='change_input_purple'><br>";
        echo "<input name='submit' type='submit' value='Confirm' class='conf_btn_purple this_btn really_this'>";
      ?>
    </div>
  </body>
</html>
