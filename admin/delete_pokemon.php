<?php
include "../includes/functions.php";
include "admin_functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <script src='../js/jquery-3.3.1.min.js'></script>
  <script src='../js/main.js'></script>
  <link rel="stylesheet" href="../css/admin.css">
  <link rel="icon" href="../stuff/pokeball.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <title>Pokedex</title>
</head>
  <body>
    <img id='nav_icon' src='../stuff/nav_icon.png'>
    <a href='admin.php'><button id='back_btn'>BACK</button></a>
    <div id='bg_nav'>
      <?php echo "<h1 id='username'>{$_SESSION['username']}</h1>"; ?>
      <nav>
        <ul>
          <li class='lvl1'>Admins</li>
          <ul>
            <a href='admin.php'><li class='lvl2'>Dashboard</li></a>
            <a href='all_admins.php'><li class='lvl2'>All Admins</li></a>
          </ul>
        </ul>
        <ul>
          <li class='lvl1'>Users</li>
          <ul>
            <a href='all_users.php'><li class='lvl2'>All Users</li></a>
            <a href='change_username.php'><li class='lvl2'>Change Username</li></a>
            <a href='change_password.php'><li class='lvl2'>Change Password</li></a>
          </ul>
          <li class='lvl1'>Pokemons</li>
          <ul>
            <a href='all_pokemons.php'><li class='lvl2'>All Pokemons</li></a>
            <a href='create_pokemons.php'><li class='lvl2'>Create New Pokemon</li></a>
            <a href='all_types.php'><li class='lvl2'>All Pokemon Types</li></a>
            <a href='create_types.php'><li class='lvl2'>Create New Type</li></a>
          </ul>
        </ul>
      </nav>
    </div>

    <div class='content'>
      <h2>Delete</h2>
      <?php
      if (isset($_POST['submit'])) {
        $query = "DELETE FROM pokemons WHERE id={$_GET['id']}";
        $result = mysqli_query($conn,$query);

        $query = "DELETE FROM pokemons_types WHERE id_pokemon={$_GET['id']}";
        $result = mysqli_query($conn,$query);

        $query = "DELETE FROM pokemon_weaknesses WHERE id_pokemon={$_GET['id']}";
        $result = mysqli_query($conn,$query);

        header('Location: all_pokemons.php');
      }
        echo "<table><tr><th>ID</th><th>NAME</th><th>EVOLUTION</th><th>TYPES</th><th>WEAKNESSES</th></tr>";
        echo "<tr><td>{$_GET['id']}</td><td>{$_GET['name']}</td><td>{$_GET['evolution']}</td>";
        //TYPES
        $query = "SELECT types.name
                  FROM pokemons_types
                  JOIN types ON types.id=pokemons_types.id_types
                  JOIN pokemons ON pokemons.id=pokemons_types.id_pokemon
                  WHERE pokemons.id={$_GET['id']}";
        $result = mysqli_query($conn,$query);
        echo "<td><select class='all_pokemons_select' readonly>";
        while ($row = mysqli_fetch_row($result)) {
            foreach ($row as $value) {
              echo "<option>{$value}</option>";
            }

        }
        echo "</select></td>";
        //WEAKNESSES
        $query = "SELECT types.name
                    FROM pokemon_weaknesses
                    JOIN types ON types.id=pokemon_weaknesses.id_weaknesses
                    JOIN pokemons ON pokemons.id=pokemon_weaknesses.id_pokemon
                    WHERE pokemons.id={$_GET['id']}";
        $result = mysqli_query($conn,$query);
        echo "<td><select class='all_pokemons_select' readonly>";
        while($row = mysqli_fetch_row($result)) {
            foreach ($row as $value) {
                echo "<option>{$value}</option>";
            }
        }
        echo "</select></td></tr></table>";
        echo "<form method='post'><input class='conf_btn_purple this_btn really_this' name='submit' type='submit' value='Confirm'></form>";
      ?>
    </div>
  </body>
</html>
