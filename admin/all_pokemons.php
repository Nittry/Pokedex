<?php
include "../includes/functions.php";
include "admin_functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <script src='../js/jquery-3.3.1.min.js'></script>
  <script src='../js/main.js'></script>
  <link rel="stylesheet" href="../css/admin.css">
  <link rel="icon" href="../stuff/pokeball.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <title>Pokedex</title>
</head>
  <body>
    <img id='nav_icon' src='../stuff/nav_icon.png'>
    <a href='../index.php'><button id='back_btn'>BACK</button></a>
    <div id='bg_nav'>
      <?php echo "<h1 id='username'>{$_SESSION['username']}</h1>"; ?>
      <nav>
        <ul>
          <li class='lvl1'>Admins</li>
          <ul>
            <a href='admin.php'><li class='lvl2'>Dashboard</li></a>
            <a href='all_admins.php'><li class='lvl2'>All Admins</li></a>
          </ul>
        </ul>
        <ul>
          <li class='lvl1'>Users</li>
          <ul>
            <a href='all_users.php'><li class='lvl2'>All Users</li></a>
            <a href='change_username.php'><li class='lvl2'>Change Username</li></a>
            <a href='change_password.php'><li class='lvl2'>Change Password</li></a>
          </ul>
          <li class='lvl1'>Pokemons</li>
          <ul>
            <a href='all_pokemons.php'><li class='lvl2 active'>All Pokemons</li></a>
            <a href='create_pokemons.php'><li class='lvl2'>Create New Pokemon</li></a>
            <a href='all_types.php'><li class='lvl2'>All Pokemon Types</li></a>
            <a href='create_types.php'><li class='lvl2'>Create New Type</li></a>
          </ul>
        </ul>
      </nav>
    </div>

    <div class='content'>
      <h2>All Pokemons</h2>
      <?php
      $query = "SELECT id AS 'all_ids' FROM pokemons";
      $result = mysqli_query($conn,$query);
      $all_ids = array();
      while($row = mysqli_fetch_assoc($result)){
      array_push($all_ids,$row['all_ids']);
      }
      echo "<table><form><tr><th>ID</th><th>NAME</th><th>EVOLUTION</th><th>TYPES</th><th>WEAKNESSES</th><th></th><th></th></tr>";
      for($i=1; $i < count($all_ids); $i++) {
      //ID, NAME, EVOLUTION
      $types = array();
      $weaknesses = array();
      $query = "SELECT pokemons.id,pokemons.name, pokemons.evolution FROM pokemons WHERE id=$all_ids[$i]";
      $result = mysqli_query($conn,$query);
      while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['id'];
        $name = $row['name'];
        $evolution = $row['evolution'];
        echo "<tr><td>$id</td><td>$name</td><td>$evolution</td>";
      }
      //TYPES
      $query = "SELECT types.name
                FROM pokemons_types
                JOIN types ON types.id=pokemons_types.id_types
                JOIN pokemons ON pokemons.id=pokemons_types.id_pokemon
                WHERE pokemons.id=$all_ids[$i]";
      $result = mysqli_query($conn,$query);
      echo "<td><select class='all_pokemons_select' readonly>";
      while ($row = mysqli_fetch_row($result)) {
          foreach ($row as $value) {
            echo "<option>{$value}</option>";
          }

      }
      echo "</select></td>";
      //WEAKNESSES
      $query = "SELECT types.name
                  FROM pokemon_weaknesses
                  JOIN types ON types.id=pokemon_weaknesses.id_weaknesses
                  JOIN pokemons ON pokemons.id=pokemon_weaknesses.id_pokemon
                  WHERE pokemons.id=$all_ids[$i]";
      $result = mysqli_query($conn,$query);
      echo "<td><select class='all_pokemons_select' readonly>";
      while($row = mysqli_fetch_row($result)) {
          foreach ($row as $value) {
              echo "<option>{$value}</option>";
          }
      }
      echo "</select></td>";
      echo "
      <td><a href='edit_pokemon.php?id={$all_ids[$i]}&name={$name}&evolution={$evolution}' class='tde'><img class='edit' src='../stuff/edit.png'>Edit</a></td>
      <td><a href='delete_pokemon.php?id={$all_ids[$i]}&name={$name}&evolution={$evolution}' class='tdd'><img class='trash' src='../stuff/trash.png'>Delete</a></td></tr>
      ";
    }
    echo "</table></form>";
      ?>
    </div>
  </body>
</html>
