<?php
include "../includes/functions.php";
include "admin_functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <script src='../js/jquery-3.3.1.min.js'></script>
  <script src='../js/main.js'></script>
  <link rel="stylesheet" href="../css/admin.css">
  <link rel="icon" href="../stuff/pokeball.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <title>Pokedex</title>
</head>
  <body>
    <img id='nav_icon' src='../stuff/nav_icon.png'>
    <a href='../index.php'><button id='back_btn'>BACK</button></a>
    <div id='bg_nav'>
      <?php echo "<h1 id='username'>{$_SESSION['username']}</h1>"; ?>
      <nav>
        <ul>
          <li class='lvl1'>Admins</li>
          <ul>
            <a href='admin.php'><li class='lvl2'>Dashboard</li></a>
            <a href='all_admins.php'><li class='lvl2'>All Admins</li></a>
          </ul>
        </ul>
        <ul>
          <li class='lvl1'>Users</li>
          <ul>
            <a href='all_users.php'><li class='lvl2'>All Users</li></a>
            <a href='change_username.php'><li class='lvl2'>Change Username</li></a>
            <a href='change_password.php'><li class='lvl2'>Change Password</li></a>
          </ul>
          <li class='lvl1'>Pokemons</li>
          <ul>
            <a href='all_pokemons.php'><li class='lvl2'>All Pokemons</li></a>
            <a href='create_pokemons.php'><li class='lvl2'>Create New Pokemon</li></a>
            <a href='all_types.php'><li class='lvl2 active'>All Pokemon Types</li></a>
            <a href='create_types.php'><li class='lvl2'>Create New Type</li></a>
          </ul>
        </ul>
      </nav>
    </div>

    <div class='content'>
      <h2>All Pokemon Types</h2>
      <?php
        $query = "SELECT * FROM types;";
        $result = mysqli_query($conn,$query);
        //Echo-ing all users into a table
        echo "<table><tr><th>ID</th><th>TYPES</th><th></th><th></th></tr>";
        while ($row = mysqli_fetch_assoc($result)) {
          echo "
                <td>{$row['id']}</td>
                <td>{$row['name']}</td>
                <td><a href='edit_types.php?id={$row['id']}' class='tde'><img class='edit' src='../stuff/edit.png'>Edit</a></td>
                <td><a href='delete_types.php?id={$row['id']}' class='tdd'><img class='trash' src='../stuff/trash.png'>Delete</a></td></tr>
          ";
        }
      ?>
    </div>
  </body>
</html>
