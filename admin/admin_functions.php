<?php
  error_reporting(0);
  include "../includes/db.php"
?>
<?php
function stats(){ //Function for stats in admin.php
  global $conn;
  //Number of pokemons in database
  $query = "SELECT COUNT(pokemons.id) AS 'pokemons' FROM pokemons;";
  $result = mysqli_query($conn,$query);
  $row = mysqli_fetch_assoc($result);
  echo "<h3>Pokemons created: <span>{$row['pokemons']}</span></h3>";
  //Admins registered
  $query = "SELECT COUNT(users.id) AS 'admins' FROM users WHERE admin=1";
  $result = mysqli_query($conn,$query);
  $row = mysqli_fetch_assoc($result);
  echo "<h3>Admins registered: <span>{$row['admins']}</span></h3>";
  //Users registered
  $query = "SELECT COUNT(users.id) AS 'users' FROM users WHERE admin=0";
  $result = mysqli_query($conn,$query);
  $row = mysqli_fetch_assoc($result);
  echo "<h3>Users registered: <span>{$row['users']}</span></h3>";
}
function allAdmins(){ //Function for selecting all admin in all_admins.php
  global $conn;
  //Select all admins
  $query = "SELECT * FROM users WHERE admin=1;";
  $result = mysqli_query($conn,$query);
  //Echo-ing all admins into a table
  echo "<table><tr><th>ID</th><th>USERNAME</th><th>ADMIN</th><th></th><th></th></tr>";
  while ($row = mysqli_fetch_assoc($result)) {
    echo "<tr><td>{$row['id']}</td>
              <td>{$row['username']}</td>
              <td>{$row['admin']}</td>
              <td><a href='edit.php?id={$row['id']}' class='tde'><img class='edit' src='../stuff/edit.png'>Edit</a></td>
              <td><a href='delete.php?id={$row['id']}' class='tdd'><img class='trash' src='../stuff/trash.png'>Delete</a></td></tr>";
  }
}
function allUsers(){ //Function for selecting all users in all_users.php
  global $conn;
  //Select all users
  $query = "SELECT * FROM users WHERE admin=0;";
  $result = mysqli_query($conn,$query);
  //Echo-ing all users into a table
  echo "<table><tr><th>ID</th><th>USERNAME</th><th>ADMIN</th><th></th><th></th></tr>";
  while ($row = mysqli_fetch_assoc($result)) {
    echo "<tr><td>{$row['id']}</td>
              <td>{$row['username']}</td>
              <td>{$row['admin']}</td>
              <td><a href='edit.php?id={$row['id']}' class='tde'><img class='edit' src='../stuff/edit.png'>Edit</a></td>
              <td><a href='delete.php?id={$row['id']}' class='tdd'><img class='trash' src='../stuff/trash.png'>Delete</a></td></tr>";
  }
}
function changePassword(){ //Function for changing password in admin - change_password and user - change_password
  global $conn;
  if (isset($_POST['submit'])) { //If the submit button was pressed, do this:
    $error = false;
    $current_pass = mysqli_real_escape_string($conn,$_POST['current_pass']); //Declaring current password + SQL Injection
    $pass1 = mysqli_real_escape_string($conn,$_POST['pass1']); //Declaring 1st password + SQL Injection
    $pass2 = mysqli_real_escape_string($conn,$_POST['pass2']); //Declaring 2nd password + SQL Injection
    $query = "SELECT password FROM users WHERE username = '{$_SESSION['username']}';"; // Selecting users current password
    $result = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($result);

    if (!password_verify($current_pass,$row['password'])) { // Verifing current password
      echo "<p class='err'>Your current password is invalid.</p>";
      $error = true;
    }
    if (empty($pass1) || empty($pass2)) { // Condition for empty password
      echo "<p class='err'>New password cannot be empty.</p>";
      $error = true;
    }
    if (preg_match('/\s/',$pass1) || preg_match('/\s/',$pass2)) { //Condition for white-spaces
      echo "<p class='err'>Your password cannot contain white spaces.</p>";
      $error = true;
    }
    if($pass1 !== $pass2){ //Condition for password match
      echo "<p class='err'>Passwords don't match.</p>";
      $error = true;
    }

    if(!$error){ //If all conditions are ok, do this: Update password in a database
      $pass1 = password_hash($pass1, PASSWORD_BCRYPT);
      $query = "UPDATE users SET password='{$pass1}' WHERE username='{$_SESSION['username']}'";
      $result = mysqli_query($conn,$query);
      if ($result) {
        echo "<p class='succ'>Your password has been changed.</p>";
      } else {
        echo "<p class='err'>ERROR</p>";
      }

    }

  }
  // Echo-ing Form
  echo "<form method='post'>
          <label class='change_label_purple'>Your current password</label>
          <input class='change_input_purple this_inpt' name=current_pass type=password>
          <label class='change_label_purple'>Your new password</label>
          <input class='change_input_purple this_inpt' name='pass1' type='password'>
          <label class='change_label_purple'>Your new password again</label>
          <input class='change_input_purple this_inpt' name='pass2' type='password'>
          <input class='conf_btn_purple this_btn' name='submit' type='submit' value='Confirm'>
          </form>
  ";
}
function changeUsername(){ //Function for changing username in admin - change_password and user - change_password
  global $conn;
  if(isset($_POST['submit'])){ //If the submit button was pressed, do this:
    $username = mysqli_real_escape_string($conn,$_POST['username']); //Declaring username + SQL Injection
    $query = "SELECT username FROM users WHERE username ='{$username}'"; //Selecting username from database
    $result = mysqli_query($conn,$query);
    $error = false;
    $row = mysqli_fetch_assoc($result);
      if($row['username'] == $username){ //Condition for existing username
        echo "<p class='err'>This username already exists!</p>";
        $error = true;
      }
      if(empty($username)){ //Condition for empty username
        echo "<p class='err'>Your username cannot be empty!</p>";
        $error = true;
      }
      if (preg_match('/\s/',$username)) { //Condition for white-spaces
        echo "<p class='err'>Your username cannot contain white spaces.</p>";
        $error = true;
      }
    if(!$error){ //If all conditions are ok, do this: Update username in a database
      $query = "UPDATE users SET username='{$username}' WHERE username='{$_SESSION['username']}'";
      $result = mysqli_query($conn,$query);
      if ($result) {
        $_SESSION['username'] = $username;
        echo "<p class='succ'>Your username has been updated.</p>";

      } else {
        echo "<p class='err'>ERROR</p>";
      }
    }
  }
  //Echo-ing form
  echo "<form method='post'>
          <label class='change_label_purple'>Your new username</label><input class='change_input_purple' name='username' type='text'>
          <input class='conf_btn_purple' name='submit' type='submit' value='Confirm'>
          </form>
  ";
}
function delete(){ //Function for deleting users
  global $conn;
  if(isset($_POST['submit'])){
    $query = "DELETE FROM users WHERE id={$_GET['id']}";
    $result = mysqli_query($conn,$query);
    if ($result) {
      echo "<p class='succ'>Data Updated.</p>";
    } else {
      echo "<p class='err'>ERROR</p>";
    }
  }
  $query = "SELECT * FROM users WHERE id={$_GET['id']}";
  $result = mysqli_query($conn,$query);
  echo "<table><tr><th>ID</th><th>USERNAME</th><th>ADMIN</th></tr>";
  while ($row = mysqli_fetch_assoc($result)) {
    echo "<tr><td>{$row['id']}</td>
              <td>{$row['username']}</td>
              <td>{$row['admin']}</td></tr>";
  }
  echo "</table><form method='post'><input class='conf_btn_purple this_btn really_this' name='submit' type='submit' value='Confirm'></form>";
}
function edit(){ //function for editing users
  global $conn;
  if(isset($_POST['submit'])){
    $username = mysqli_real_escape_string($conn,$_POST['username']);
    if (isset($_POST['admin'])) {
      $_POST['admin'] = 1;
    } else {
      $_POST['admin'] = 0;
    }
    $query = "UPDATE users SET username = '{$username}', admin = {$_POST['admin']} WHERE id={$_GET['id']} ";
    $result = mysqli_query($conn,$query);
    if ($result) {
      echo "<p class='succ this_succ'>Data Updated.</p>";
    } else {
      echo "<p class='err this_err'>ERROR</p>";
    }
  }
  $query = "SELECT * FROM users WHERE id={$_GET['id']}";
  $result = mysqli_query($conn,$query);
  echo "<form method='post'>";
  while ($row = mysqli_fetch_assoc($result)) {
    if ($row['admin'] == 1) {
      $checked = "checked";
    } else {
      $checked = "";
    }
    echo "<label class='change_label_purple'>Username</label>
          <input class='change_input_purple' name='username' type='text' value='{$row['username']}'><br>
          <label class='change_label_purple'>Admin</label>
          <input class='change_input_purple' name='admin' type='checkbox' {$checked}><br>
          <input class='conf_btn_purple this_btn really_this' name='submit' type='submit' value='Confirm'>
    ";
  }
  echo "</form>";
}
function createPokemon(){
  global $conn;
  $query = "SELECT id,name FROM types;";
  $result = mysqli_query($conn,$query);
  $types=array();
  $types_id=array();
  while($row = mysqli_fetch_assoc($result)){
        array_push($types,$row['name']);
        array_push($types_id,$row['id']);
  }

  if(isset($_POST['submit'])){
    $target_dir = "../poke_img/";
    $target_file = $target_dir . basename($_FILES['fileToUpload']['name']);
    $check = getimagesize($_FILES['fileToUpload']['tmp_name']);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $name = mysqli_real_escape_string($conn,$_POST['name']);
    $evolution = mysqli_real_escape_string($conn,$_POST['evolution']);
    $types1= mysqli_real_escape_string($conn,$_POST['types1']);
    $types2 = mysqli_real_escape_string($conn,$_POST['types2']);
    $weaknesses = array();
    foreach ($types_id as $value) {
      if(isset($_POST[$value])){
        array_push($weaknesses,$_POST[$value]);
      }
    }
    $error = false;
    if(empty($name)){
      echo "<p class='err'>Pokemon's name cannot be empty!</p>";
      $error = true;
    }
    if (preg_match('/\s/',$name)) {
      echo "<p class='err'>Pokemon's name cannot contain white spaces.</p>";
      $error = true;
    }
    if(empty($evolution)){
      echo "<p class='err'>Pokemon's evolution cannot be empty!</p>";
      $error = true;
    }
    if (preg_match('/\s/',$evolution)) {
      echo "<p class='err'>Pokemon's evolution cannot contain white spaces.</p>";
      $error = true;
    }
    // Check if image file is a actual image or fake image
    if($check !== false) {
      $uploadError = false;
    } else {
        echo "<p class='err'>File is not an image.</p>";
        $uploadError = true;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "<p class='err'>Sorry, your file is too large.</p>";
        $uploadError = true;
    }
    // Allow certain file formats
    if($imageFileType != "png") {
        echo "<p class='err'>Sorry, only PNG files are allowed.</p>";
        $uploadError = true;
    }

    if(!$error && !$uploadError){

      $query = "INSERT INTO pokemons(name, evolution) VALUES ('{$name}',{$evolution})";
      $result = mysqli_query($conn,$query);

      $query = "SELECT id FROM pokemons WHERE name='{$name}'";
      $result = mysqli_query($conn,$query);
      $row = mysqli_fetch_assoc($result);

      $query = "INSERT INTO pokemons_types(id_pokemon, id_types) VALUES ({$row['id']},{$types1})";
      $result = mysqli_query($conn,$query);
      if($types2 !== 'nothing'){
      $query = "INSERT INTO pokemons_types(id_pokemon, id_types) VALUES ({$row['id']},{$types2})";
      $result = mysqli_query($conn,$query);
      }
      foreach ($weaknesses as $value) {
        $query = "INSERT INTO pokemon_weaknesses(id_pokemon, id_weaknesses) VALUES ({$row['id']},{$value})";
        $result = mysqli_query($conn,$query);
      }
      move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir . "{$row['id']}" . ".png");
    }
  }
    echo "
          <form method='post' enctype='multipart/form-data'>
            <label class='change_label_purple'><i>Select pokemon profile image</i></label>
            <input class='change_input_purple purple_img' type='file' name='fileToUpload' id='fileToUpload'>
            <label class='change_label_purple'>Pokemon's name</label>
            <input class='change_input_purple this_inpt' name='name' type='text'>
            <label class='change_label_purple'>Evolution</label>
            <input class='change_input_purple number_input this_inpt' name='evolution' type='number' min='1' max='3'>
    ";

    echo "<i class='tip'>TIP: If your pokemon has only one type, set the second select for type2 to -nothing-.</i>";
    echo "<table class='margin_help'>";
    echo "<tr><th>1st Type</th><th>2nd Type</th></tr>";
    echo "<tr><td><select class='select_inpt' name='types1'>";
    $count_id=0;
    foreach ($types as $type) {
        echo "<option class='option_inpt' value='{$types_id[$count_id]}'>{$type}</option>";
        $count_id++;
      }
    echo "</select></td>";
    echo "<td><select class='select_inpt' name='types2'>";
    echo "<option class='option_inpt' value='nothing'>nothing</option>";
    $count_id=0;
    foreach ($types as $type) {
        echo "<option value='{$types_id[$count_id]}'>{$type}</option>";
        $count_id++;
      }
    echo "</select></td></tr></table>";
    echo "</table><table class='margin_help'>";
    echo "<i class='tip'>TIP: Every pokemon can have 0-5 weaknesses.</i>";
    echo "<tr><th colspan='2'>Weaknesses</th><th></th></tr>";
    $count_id=0;
    foreach ($types as $type) {
        echo "<tr><td class='box_td'><input class='box' type='checkbox' name='{$types_id[$count_id]}' value='{$types_id[$count_id]}'></td><td>{$type}</td></tr>";
        $count_id++;
      }
    echo "</table>";
    echo "<input class='conf_btn_purple this_btn margin_help' name='submit' type='submit' value='Create new pokemon'></form>";

}
?>
