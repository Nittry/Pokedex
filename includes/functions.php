<?php
error_reporting(0);
include 'db.php';
session_start();
function register(){
  global $conn;
  if (isset($_POST['submit'])) {
    $username = mysqli_real_escape_string($conn,$_POST['username']); // Preventing SQL Injection + Declaring username into variable
    $password = mysqli_real_escape_string($conn,$_POST['password']); // Preventing SQL Injection + Declaring password into variable
    $error = false;

    if (!$conn) { // Error for failed connection
      echo "<p id='error'>Error in connection!</p>";
      $error = true;
    }
    if(empty($username)) { // Error for empty username
      echo "<p id='error'>You need to fill your username!</p>";
      $error = true;
    }
    if(empty($password)){ // Error for empty password
      echo "<p id='error'>You need to fill your password!</p>";
      $error = true;
    }
    if (preg_match('/\s/',$username)) { // Erro for white spaces in username
      echo "<p class='error'>Your username cannot contain white spaces.</p>";
      $error = true;
    }
    if (preg_match('/\s/',$password)) { // Erro for white spaces in username
      echo "<p class='error'>Your password cannot contain white spaces.</p>";
      $error = true;
    }
    if(strlen($username) < 3){ // Error for username shorter than 3 characters
      echo "<p id='error'>Your username has to be longer than 3 characters!</p>";
      $error = true;
    }
    if(strlen($username) > 12){ // Error for username longer than 12 characters
      echo "<p id='error'>Your username cannot be longer than 12 characters!</p>";
      $error = true;
    }
    if(strlen($password) < 8){ // Error for password shorter than 8
      echo "<p id='error'>Your password cannot be shorter than 8 characters!</p>";
      $error = true;
    }
    if(strlen($password) > 50){ // Error for password longer than 50 characters
      echo "<p id='error'>Your password cannot be longer than 50 characters!</p>";
      $error = true;
    }
      $query = "SELECT username FROM users WHERE username='$username';";
      $result = mysqli_query($conn,$query);
      while($row = mysqli_fetch_assoc($result)) {
        if($row['username'] == $username){
          echo "<p id='error'>This username already exists!</p>";
          $error = true;
        }
      }
    if(!$error){
      $password = password_hash($password, PASSWORD_BCRYPT);
      $query = "INSERT INTO users (username,password) VALUES ('$username','$password');";
      $result = mysqli_query($conn,$query);
      if ($result) {
        echo "<p id='success'>User registred successfully.</p>";
      }
    }
  }
}

function login(){
  global $conn;
  if(isset($_POST['submit'])){
    $username = mysqli_real_escape_string($conn,$_POST['username']); // Preventing SQL Injection + Declaring username into variable
    $password = mysqli_real_escape_string($conn,$_POST['password']); // Preventing SQL Injection + Declaring password into variable
    if (!$conn) { // Error for failed connection
    echo "<p id='error'>Error in connection!</p>";
    }
    $query = "SELECT id, password, admin FROM users WHERE username='$username'";
    $result = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($result);
    if (password_verify($password,$row['password'])) {
      $_SESSION['username'] = $username;
      $_SESSION['admin'] = $row['admin'];
      $_SESSION['id'] = $row['id'];
      header('Location: index.php');
    } else {
      echo "<p id='error'>Invalid username or password.</p>";
    }
  }
}
function indexHeader(){
  if(isset($_SESSION['username'])) {
    echo "
          <div class='fixed_dropdown'>
            <p id='log_username'>{$_SESSION['username']}</p>
            <div class='dropdown'>
              <img id='profilePic' src='stuff/profile_pic.png'>
              <img id='logout_arrow' src='stuff/down_arrow.png'>
                <div class='dropdown-content'>
                  <form method='post'>
                    <input class='form_butt' name='profile' type='submit' value='Profile'>
                    <input class='form_butt' name='logout' type='submit' value='Logout'>
                  </form>
                </div>
            </div>
          </div>

          <a href='index.php'><img id='head-pic' src='stuff/pokémon.png'></a>
          ";
  }else{
    echo "
          <div class='connection'>
          <a href='login.php' id='login' class='btn'>Login</a>
          <a href='register.php' id='register' class='btn'>Register</a>
          </div>
          <a href='index.php'><img id='head-pic' src='stuff/pokémon.png'></a>
         ";
  }
  if(isset($_POST['logout'])){
    unset($_SESSION["username"]);
    unset($_SESSION["id"]);
    header('Location: index.php');
  }
  if(isset($_POST['profile'])){
    if($_SESSION['admin']== 1){
      header('Location: admin/admin.php');
    }else{
      header('Location: user/profile.php');
    }
  }
}
function allPokemons(){
  global $conn;
  $query = "SELECT id,name FROM pokemons;";
  $result = mysqli_query($conn,$query);
  $count = 0;
  // echo "<div class='row'>";
  while ($row = mysqli_fetch_assoc($result)) {
    // if ($count == 5) {
    //   echo "</div><div class='row'>";
    //   $count = 0;
    // }
    echo "
          <a href='pokemon.php?id={$row['id']}'><div class='fifth'>
          <img class='poke_img' src='poke_img/{$row["id"]}.png'>
          <p class='poke_name'>{$row['name']}</p>
          </div></a>
        ";
    $count++;
  }
}
?>
