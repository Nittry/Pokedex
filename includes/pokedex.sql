-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 08. kvě 2018, 17:37
-- Verze serveru: 10.1.29-MariaDB
-- Verze PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `pokedex`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `pokemons`
--

CREATE TABLE `pokemons` (
  `id` int(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `evolution` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `pokemons`
--

INSERT INTO `pokemons` (`id`, `name`, `evolution`) VALUES
(1, 'Bulbasaur', 1),
(2, 'Ivysaur', 2),
(3, 'Venusaur', 3),
(4, 'Charmander', 1),
(5, 'Charmaleon', 2),
(6, 'Charizard', 3),
(7, 'Squirtle', 1),
(8, 'Wartortle', 2),
(9, 'Blastoise', 3),
(10, 'Caterpie', 1),
(11, 'Metapod', 2),
(12, 'Butterfree', 3),
(13, 'Weedle', 1),
(14, 'Kakuna', 2),
(15, 'Beedrill', 3),
(16, 'Pidgey', 1),
(17, 'Pidgeotto', 2),
(18, 'Pidgeot', 3),
(19, 'Ratatta', 1),
(20, 'Raticade', 2),
(21, 'Spearow', 1),
(22, 'Fearow', 2),
(23, 'Ekans', 1),
(24, 'Arbok', 2),
(25, 'Pikachu', 2),
(26, 'Raichu', 3),
(27, 'Sandshrew', 1),
(28, 'Sandslash', 2),
(29, 'Nidoran', 1),
(30, 'Nidorina', 2),
(31, 'Nidoqueen', 3),
(32, 'Nidoran', 1),
(33, 'Nidorino', 2),
(34, 'Nidoking', 3),
(35, 'Clefairy', 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `pokemons_types`
--

CREATE TABLE `pokemons_types` (
  `id` int(3) NOT NULL,
  `id_pokemon` int(3) NOT NULL,
  `id_types` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `pokemons_types`
--

INSERT INTO `pokemons_types` (`id`, `id_pokemon`, `id_types`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 2),
(4, 2, 3),
(5, 3, 2),
(6, 3, 3),
(7, 4, 4),
(8, 5, 4),
(9, 6, 4),
(10, 6, 6),
(11, 7, 5),
(12, 8, 5),
(13, 9, 5),
(14, 10, 7),
(15, 11, 7),
(16, 12, 6),
(17, 12, 7),
(22, 15, 3),
(23, 15, 7),
(24, 16, 1),
(25, 16, 6),
(26, 17, 1),
(27, 17, 6),
(28, 18, 1),
(29, 18, 6),
(30, 19, 1),
(31, 20, 1),
(32, 21, 1),
(33, 21, 6),
(34, 22, 1),
(35, 22, 6),
(36, 23, 3),
(37, 24, 3),
(38, 25, 8),
(39, 26, 8),
(40, 27, 9),
(41, 28, 9),
(43, 30, 3),
(44, 31, 3),
(45, 31, 9),
(47, 33, 3),
(48, 34, 3),
(49, 34, 9),
(50, 14, 3),
(51, 14, 7),
(52, 32, 3),
(59, 35, 10),
(60, 13, 3),
(61, 13, 7),
(62, 29, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `pokemon_weaknesses`
--

CREATE TABLE `pokemon_weaknesses` (
  `id` int(3) NOT NULL,
  `id_pokemon` int(3) NOT NULL,
  `id_weaknesses` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `pokemon_weaknesses`
--

INSERT INTO `pokemon_weaknesses` (`id`, `id_pokemon`, `id_weaknesses`) VALUES
(1, 1, 4),
(2, 1, 6),
(3, 1, 15),
(4, 1, 12),
(5, 2, 4),
(6, 2, 6),
(7, 2, 12),
(8, 2, 15),
(9, 3, 4),
(10, 3, 6),
(11, 3, 12),
(12, 3, 15),
(13, 4, 9),
(14, 4, 13),
(15, 4, 5),
(16, 5, 9),
(17, 5, 13),
(18, 5, 5),
(19, 6, 8),
(20, 6, 13),
(21, 6, 5),
(22, 7, 2),
(23, 7, 8),
(24, 8, 2),
(25, 8, 8),
(26, 9, 2),
(27, 9, 8),
(28, 10, 4),
(29, 10, 6),
(30, 10, 13),
(31, 11, 4),
(32, 11, 6),
(33, 11, 13),
(34, 12, 8),
(35, 12, 13),
(36, 12, 4),
(37, 12, 6),
(38, 12, 15),
(43, 14, 4),
(44, 14, 6),
(45, 14, 12),
(46, 14, 13),
(47, 15, 4),
(48, 15, 6),
(49, 15, 12),
(50, 15, 13),
(51, 16, 8),
(52, 16, 15),
(53, 16, 13),
(54, 17, 8),
(55, 17, 15),
(56, 17, 13),
(57, 18, 8),
(58, 18, 15),
(59, 18, 13),
(60, 19, 11),
(61, 20, 11),
(62, 21, 8),
(63, 21, 15),
(64, 21, 13),
(65, 22, 8),
(66, 22, 15),
(67, 22, 13),
(68, 23, 9),
(69, 23, 12),
(70, 24, 9),
(71, 24, 12),
(72, 25, 9),
(73, 26, 9),
(74, 27, 2),
(75, 27, 15),
(76, 27, 5),
(77, 28, 2),
(78, 28, 15),
(79, 28, 5),
(82, 30, 9),
(83, 30, 12),
(84, 31, 5),
(85, 31, 9),
(86, 31, 12),
(87, 31, 15),
(90, 33, 9),
(91, 33, 12),
(92, 34, 5),
(93, 34, 9),
(94, 34, 12),
(95, 34, 15),
(96, 32, 9),
(97, 32, 12),
(107, 35, 3),
(108, 35, 14),
(109, 13, 4),
(110, 13, 6),
(111, 13, 12),
(112, 13, 13),
(113, 29, 9),
(114, 29, 12);

-- --------------------------------------------------------

--
-- Struktura tabulky `types`
--

CREATE TABLE `types` (
  `id` int(3) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `types`
--

INSERT INTO `types` (`id`, `name`) VALUES
(1, 'normal'),
(2, 'grass'),
(3, 'poison'),
(4, 'fire'),
(5, 'water'),
(6, 'flying'),
(7, 'bug'),
(8, 'electric'),
(9, 'ground'),
(10, 'fairy'),
(11, 'fighting'),
(12, 'psychic'),
(13, 'rock'),
(14, 'steel'),
(15, 'ice'),
(16, 'dragon'),
(17, 'dark'),
(18, 'ghost');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `admin`) VALUES
(1, 'admin', '$2y$10$guueosHefTz0Iv8k1Cl07.gCxFSZA7hiIborxf3EQ4LgEkHN7rd5C', 1),
(2, 'user1', '$2y$10$Z/E.UluSH96iQwLef7C61OgPrLEOy18/JgPbE0B9RWgF4RiWHuLVm', 0),
(3, 'Nittry', '$2y$10$JksCsRAE8XTJp5qczm/ypuJlljQfagOwZI3kVs1ttVCyfjYhCjZU2', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `user_pokemons`
--

CREATE TABLE `user_pokemons` (
  `id` int(3) NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_pokemon` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `user_pokemons`
--

INSERT INTO `user_pokemons` (`id`, `id_user`, `id_pokemon`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 1, 1),
(4, 1, 2),
(6, 1, 4),
(7, 2, 10),
(8, 1, 3),
(10, 2, 4);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `pokemons`
--
ALTER TABLE `pokemons`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `pokemons_types`
--
ALTER TABLE `pokemons_types`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `pokemon_weaknesses`
--
ALTER TABLE `pokemon_weaknesses`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Klíče pro tabulku `user_pokemons`
--
ALTER TABLE `user_pokemons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `pokemons`
--
ALTER TABLE `pokemons`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pro tabulku `pokemons_types`
--
ALTER TABLE `pokemons_types`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT pro tabulku `pokemon_weaknesses`
--
ALTER TABLE `pokemon_weaknesses`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT pro tabulku `types`
--
ALTER TABLE `types`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `user_pokemons`
--
ALTER TABLE `user_pokemons`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
