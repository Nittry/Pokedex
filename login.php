<?php
include "includes/functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Pokédex</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/login.css">
    <link rel="icon" href="stuff/pokeball.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <div class="content">
      <img id="head-pic" src="stuff/pokémon.png">
      <a href="index.php" id="back"><img class="small_icon" src="stuff/back_arrow.png">BACK</a>
      <div class="connect">
        <div class="login">
          <form action="login.php" method="post">
            <label>Username</label>
            <input id="ipt" name="username" type="text" placeholder="Username" required>
            <label>Password</label>
            <input id="ipt" name="password" type="password" placeholder="Password" required>

            <div class="space">
              <?php login(); ?>
              <p id="error" class="hidden">Error: invalid username or password</p>
            </div>

            <input name="submit" class="btn" id="login" type="submit" value="Login">
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
