<?php
include "includes/functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Pokedex</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/filter.css">
    <link rel="icon" href="stuff/pokeball.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <header>
        <?php indexHeader(); ?>
        <a class='main_head_text'>My collection</a>
    </header>

      <a href='filter.php'><button id='filter_btn' class='btn'>Filter pokemons</button></a>
      <a href='index.php'><button id='colletion_btn' class='btn'>All pokemons</button></a>

      <form action="search.php" id='searchform' method='post'>
        <input class='search btn' id='pokebtn'  type='submit' name='search_submit' value='Search'>
        <input class='search' id='pokeinput' type='text' name='search' placeholder='Search pokemon'>
      </form>
    <div class="content">
      <!-- <div class='row'>
        <a href='pokemon.php?id=1'>
        <div class='fifth'>
          <img class='poke_img' src='poke_img/1.png'>
          <p class='poke_name'>Bulbasaur</p>
        </div></a>

        <a href='pokemon.php?id=1'>
        <div class='fifth'>
          <img class='poke_img' src='poke_img/1.png'>
          <p class='poke_name'>Bulbasaur</p>
        </div></a>

        <a href='pokemon.php?id=1'>
        <div class='fifth'>
          <img class='poke_img' src='poke_img/1.png'>
          <p class='poke_name'>Bulbasaur</p>
        </div></a>

        <a href='pokemon.php?id=1'>
        <div class='fifth'>
          <img class='poke_img' src='poke_img/1.png'>
          <p class='poke_name'>Bulbasaur</p>
        </div></a>

        <a href='pokemon.php?id=1'>
        <div class='fifth'>
          <img class='poke_img' src='poke_img/1.png'>
          <p class='poke_name'>Bulbasaur</p>
        </div></a>

      </div> -->

      <?php
      $query = "SELECT pokemons.id,pokemons.name
                FROM pokemons
                JOIN user_pokemons ON pokemons.id=user_pokemons.id_pokemon
                WHERE user_pokemons.id_user={$_SESSION['id']} ORDER BY pokemons.id";
      $result = mysqli_query($conn,$query);
      $count = 0;
      echo "<div class='row'>";
      while ($row = mysqli_fetch_assoc($result)) {
        if ($count == 5) {
          echo "</div><div class='row'>";
          $count = 0;
        }
        echo "
              <a href='pokemon.php?id={$row['id']}'><div class='fifth'>
              <img class='poke_img' src='poke_img/{$row["id"]}.png'>
              <p class='poke_name'>{$row['name']}</p>
              </div></a>
            ";
        $count++;
      }
      ?>

    </div>
