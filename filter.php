<?php
include "includes/functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Pokedex</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/filter.css">
    <link rel="icon" href="stuff/pokeball.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <header>
        <?php indexHeader(); ?>
    </header>

    <div class='content filter_cont'>
      <?php
      //Putting all types into arrays
      $types_names = array();
      $types_ids = array();
      $query = "SELECT * FROM types;";
      $result = mysqli_query($conn,$query);
      while ($row = mysqli_fetch_assoc($result)) {
        array_push($types_names,$row['name']);
        array_push($types_ids,$row['id']);
      }
        //Echo-ing table, form, user's pokemon, pokemon's name, evolution
        echo "
              <form action='search.php' method='get'>
                <table>
                  <tr>
                    <th colspan='2'>Pokemon filter by it's information:</th>
                    <th></th>
                  </tr>
                  <tr>
                    <td><label class='filter_label'>User's name</label></td>
                    <td><input class='filter_input' name='user_name' type='text'></td>
                  </tr>
                  <tr>
                    <td><label class='filter_label'>Pokemon's name</label></td>
                    <td><input class='filter_input' name='pokemon_name' type='text'></td>
                  </tr>
                  <tr>
                    <td><label class='filter_label'>Evolution</label></td>
                    <td><input class='filter_input number_input' name='evolution' type='number' min='1' max='3'></td>
                  </tr>
        ";
        //Echo-ing select for types
        echo "<tr>
                <td><label class='filter_label'>Type</label></td>
                <td><select class='type_select' name='types'>
                <option class='type_option'></option>
        ";
        //Echo-ing all types into select options
        for ($i=0; $i < count($types_ids); $i++) {
          echo "<option class='type_option' value='{$types_ids[$i]}'>{$types_names[$i]}</option>";
        }
        echo "</select></td></tr>";

        //Echo-ing select for weaknesses
        echo "<tr>
                <td><label class='filter_label'>Weaknesses</label></td>
                <td><select class='type_select' name='weaknesses'>
                <option class='type_option'></option>
        ";
        //Echo-ing all weaknesses into select options
        for ($i=0; $i < count($types_ids); $i++) {
          echo "<option class='type_option' value='{$types_ids[$i]}'>{$types_names[$i]}</option>";
        }
        echo "</select></td></tr>";

        //Echo-ing close tags
        echo "
            </table>
            <input class='submit_btn' id='filter_butt' name='filter' type='submit' value='Find Pokemon'>
          </form>
        ";
      ?>
    </div>
  </body>
</html>
