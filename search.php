<?php
include "includes/functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Pokedex</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/index.css">
    <link rel="icon" href="stuff/pokeball.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <header>
        <?php indexHeader(); ?>
    </header>

    <form id='searchform' method='post'>
      <input class='search btn' id='pokebtn'  type='submit' name='search_submit' value='Search'>
      <input class='search' id='pokeinput' type='text' name='search' placeholder='Search pokemon'>
    </form>

    <a href='index.php'><button id='back_btn'>BACK</button></a>

    <div class="content">
      <?php
        if(isset($_POST['search_submit'])){
          $search = mysqli_real_escape_string($conn,$_POST['search']);
          $query = "SELECT * FROM pokemons WHERE pokemons.name LIKE '%$search%' ";
          $result = mysqli_query($conn,$query);
          $count = mysqli_num_rows($result);
          if($count == 0){
            echo "<p class='message'>{$search} is not in our database.</p>";
          }else{
            while ($row = mysqli_fetch_assoc($result)) {
              echo "
                  <a href='pokemon.php?id={$row['id']}'><div class='fifth'>
                  <img class='poke_img' src='poke_img/{$row["id"]}.png'>
                  <p class='poke_name'>{$row['name']}</p>
                  </div></a>
                ";
            }
          }
        }
        if (isset($_GET['filter'])) {
          $user_name = mysqli_real_escape_string($conn,$_GET['user_name']);
          $pokemon_name = mysqli_real_escape_string($conn,$_GET['pokemon_name']);
          $evolution = mysqli_real_escape_string($conn,$_GET['evolution']);
          $type = mysqli_real_escape_string($conn,$_GET['types']);
          $weaknesses = mysqli_real_escape_string($conn,$_GET['weaknesses']);

          $query = "SELECT DISTINCT pokemons.id,pokemons.name FROM pokemons
                    JOIN pokemons_types ON pokemons.id=pokemons_types.id_pokemon
                    JOIN pokemon_weaknesses ON pokemons.id=pokemon_weaknesses.id_pokemon
                    JOIN user_pokemons ON pokemons.id=user_pokemons.id_pokemon
                    JOIN users ON user_pokemons.id_user=users.id
                    WHERE pokemons_types.id_types LIKE '%{$type}%'
                    AND pokemons.name LIKE '%{$pokemon_name}%'
                    AND pokemons.evolution LIKE '%{$evolution}%'
                    AND pokemon_weaknesses.id_weaknesses LIKE '%{$weaknesses}%'
                    AND users.username LIKE '%{$user_name}%'";
          $result = mysqli_query($conn,$query);
          $count = mysqli_num_rows($result);
          if($count == 0){
            echo "<p class='message'>We cannot find anything you filtered in our database.</p>";
          }else {
            while ($row = mysqli_fetch_assoc($result)) {
              echo "
                  <a href='pokemon.php?id={$row['id']}'><div class='fifth'>
                  <img class='poke_img' src='poke_img/{$row["id"]}.png'>
                  <p class='poke_name'>{$row['name']}</p>
                  </div></a>
                ";
            }
          }
        }
        if (isset($_GET['type'])) {
          $type_name = $_GET['type'];
          $query = "SELECT DISTINCT pokemons.id,pokemons.name FROM pokemons_types
                    JOIN pokemons ON pokemons_types.id_pokemon=pokemons.id
                    JOIN types ON pokemons_types.id_types=types.id
                    WHERE types.name LIKE '%{$type_name}%'";
                    $result = mysqli_query($conn,$query);
                    $count = mysqli_num_rows($result);
                    if($count == 0){
                      echo "<p class='message'>{$type_name} is not in our database.</p>";
                    }else {
                      while ($row = mysqli_fetch_assoc($result)) {
                        echo "
                            <a href='pokemon.php?id={$row['id']}'><div class='fifth'>
                            <img class='poke_img' src='poke_img/{$row["id"]}.png'>
                            <p class='poke_name'>{$row['name']}</p>
                            </div></a>
                          ";
                      }
                    }
        }
        if (isset($_GET['weakness'])) {
          $weakness_name = $_GET['weakness'];
          $query = "SELECT DISTINCT pokemons.id,pokemons.name FROM pokemon_weaknesses
                    JOIN pokemons ON pokemon_weaknesses.id_pokemon=pokemons.id
                    JOIN types ON pokemon_weaknesses.id_weaknesses=types.id
                    WHERE types.name LIKE '%{$weakness_name}%'";
                    $result = mysqli_query($conn,$query);
                    $count = mysqli_num_rows($result);
                    if($count == 0){
                      echo "<p class='message'>{$weakness_name} is not in our database.</p>";
                    }else {
                      while ($row = mysqli_fetch_assoc($result)) {
                        echo "
                            <a href='pokemon.php?id={$row['id']}'><div class='fifth'>
                            <img class='poke_img' src='poke_img/{$row["id"]}.png'>
                            <p class='poke_name'>{$row['name']}</p>
                            </div></a>
                          ";
                      }
                    }
        }

      ?>
    </div>
  </body>
</html>
