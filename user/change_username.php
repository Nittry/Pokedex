<?php
include "../includes/functions.php";
include "../admin/admin_functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src='../js/jquery-3.3.1.min.js'></script>
    <script src='../js/main.js'></script>
    <link rel="stylesheet" href="../css/profile.css">
    <link rel="icon" href="../stuff/pokeball.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Pokedex</title>
  </head>
  <body>
    <a href='../index.php'><button id='back_btn'>BACK</button></a>
    <div id='bg_nav'>
      <?php echo "<h1 id='username'>{$_SESSION['username']}</h1>"; ?>
      <nav>
        <ul>
          <li class='lvl1'>Your stats</li>
          <ul>
            <a href='profile.php'><li class='lvl2'>Dashboard</li></a>
          </ul>
        </ul>
        <ul>
          <li class='lvl1'>Edit profile</li>
          <ul>
            <a href='change_username.php'><li class='lvl2 active'>Change Username</li></a>
            <a href='change_password.php'><li class='lvl2'>Change Password</li></a>
          </ul>
        </ul>
      </nav>
    </div>

    <div class='content'>
      <h2>Change Your Username</h2>
      <?php
        changeUsername();
      ?>
    </div>
  </body>
</html>
