<?php
include "../includes/functions.php";
include "../admin/admin_functions.php";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src='../js/jquery-3.3.1.min.js'></script>
    <script src='../js/main.js'></script>
    <link rel="stylesheet" href="../css/profile.css">
    <link rel="icon" href="../stuff/pokeball.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Pokedex</title>
  </head>
  <body>
    <img id='nav_icon' src='../stuff/nav_icon_profile.png'>
    <a href='../index.php'><button id='back_btn'>BACK</button></a>
    <div id='bg_nav'>
      <?php echo "<h1 id='username'>{$_SESSION['username']}</h1>"; ?>
      <nav>
        <ul>
          <li class='lvl1'>Your stats</li>
          <ul>
            <a href='profile.php'><li class='lvl2 active'>Dashboard</li></a>
          </ul>
        </ul>
        <ul>
          <li class='lvl1'>Edit profile</li>
          <ul>
            <a href='change_username.php'><li class='lvl2'>Change Username</li></a>
            <a href='change_password.php'><li class='lvl2'>Change Password</li></a>
          </ul>
        </ul>
      </nav>
    </div>

    <div class='content'>
      <h2>Current stats</h2>
      <?php
      //Number of owned pokemons
      $query = "SELECT COUNT(pokemons.id) AS 'pokemon_count'
                FROM pokemons
                JOIN user_pokemons ON pokemons.id=user_pokemons.id_pokemon
                WHERE user_pokemons.id_user={$_SESSION['id']}";
      $result  = mysqli_query($conn,$query);
      $row = mysqli_fetch_assoc($result);
      echo "<h3>Number of owned pokemons: <span>{$row['pokemon_count']}</span></h3>";
      //Total number of pokemons
      $query = "SELECT COUNT(pokemons.id) AS 'all_pokemon' FROM pokemons";
      $result  = mysqli_query($conn,$query);
      $row = mysqli_fetch_assoc($result);
      echo "<h3>Number of owned pokemons: <span>{$row['all_pokemon']}</span></h3>";
      ?>


      <h3>Your number of pokemons by type:</h3>
      <p>Owned pokemons by type / all pokemons of that type.</p>
      <table>
        <?php
          $query = "SELECT * FROM types";
          $result = mysqli_query($conn,$query);
          $types = array();
          while ($row = mysqli_fetch_assoc($result)) {
            array_push($types,$row['name']);
          }
          foreach ($types as $value) {
            $query = "SELECT COUNT(types.id) AS 'type_count'
                      FROM pokemons_types
                      JOIN types ON pokemons_types.id_types=types.id
                      JOIN user_pokemons ON pokemons_types.id_pokemon=user_pokemons.id_pokemon
                      WHERE user_pokemons.id_user={$_SESSION['id']} AND types.name='{$value}'";
            $result = mysqli_query($conn,$query);
            $types_count = mysqli_fetch_assoc($result);

            $query = "SELECT COUNT(pokemons_types.id_pokemon) AS 'all_types_count' FROM pokemons_types
                      JOIN pokemons ON pokemons_types.id_pokemon=pokemons.id
                      JOIN types ON pokemons_types.id_types=types.id
                      WHERE types.name='{$value}'";
            $result = mysqli_query($conn,$query);
            $all_types_count = mysqli_fetch_assoc($result);

            echo "
            <tr>
             <td><img class='icon' src='../types/{$value}.png'></td>
             <td>{$value}:</td>
             <td class='number'>{$types_count['type_count']}</td>
             <td class='overall'>{$all_types_count['all_types_count']}</td>
           </tr>
            ";
          }
        ?>
         <!--<tr>
          <td><img class='icon' src='../types/bug.png'></td>
          <td>Bug:</td>
          <td class='number'>3</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/dark.png'></td>
          <td>Dark:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/dragon.png'></td>
          <td>Dragon:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/electric.png'></td>
          <td>Electric:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/fairy.png'></td>
          <td>Fairy:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/fighting.png'></td>
          <td>Fighting:</td>
          <td class='number'>2</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/fire.png'></td>
          <td>Fire:</td>
          <td class='number'>5</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/flying.png'></td>
          <td>Flying:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/ghost.png'></td>
          <td>Ghost:</td>
          <td class='number'>2</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/grass.png'></td>
          <td>Grass:</td>
          <td class='number'>2</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/ground.png'></td>
          <td>Ground:</td>
          <td class='number'>4</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/ice.png'></td>
          <td>Ice:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/normal.png'></td>
          <td>Normal:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/poison.png'></td>
          <td>Poison:</td>
          <td class='number'>6</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/psychic.png'></td>
          <td>Psychic:</td>
          <td class='number'>1</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/rock.png'></td>
          <td>Rock:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/steel.png'></td>
          <td>Steel:</td>
          <td class='number'>10</td>
          <td class='overall'>10</td>
        </tr>
        <tr>
          <td><img class='icon' src='../types/water.png'></td>
          <td>Water:</td>
          <td class='number'>0</td>
          <td class='overall'>10</td>
        </tr> -->
      </table>
      <div id='space'></div>
    </div>
  </body>
</html>
